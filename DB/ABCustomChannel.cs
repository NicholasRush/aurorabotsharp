using System.ComponentModel.DataAnnotations.Schema;

namespace AuroraBotSharp.DB
{
    /// <summary>
    /// User channel in server custom category
    /// </summary>
    public class ABCustomChannel
    {
        [ForeignKey("Server")]
        public ulong ServerID { get; set; }
        public ABServer Server { get; set; }

        public ulong ID { get; set; }
        
        public ulong? OwnerID { get; set; }
    }
}