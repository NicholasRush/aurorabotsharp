using System.ComponentModel.DataAnnotations;

namespace AuroraBotSharp;

public enum CustomCategoryChannelVariant
{
    [Display(Name = "text")] Text,
    [Display(Name = "voice")] Voice
}