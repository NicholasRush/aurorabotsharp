﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AuroraBotSharp.DB;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AuroraBotSharp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using (var services = ConfigureServices())
            {
                //Auto-creating database
                var dbContext = services.GetRequiredService<ABDbContext>();
                dbContext.Database.EnsureCreated();

                //Logging
                var client = services.GetRequiredService<DiscordSocketClient>();
                client.Log += LogAsync;
                //services.GetRequiredService<CommandService>().Log += LogAsync;

                //Need to create BotHandlingService to properly subscribe to events
                services.GetRequiredService<BotHandlingService>();

                var commands = services.GetRequiredService<InteractionService>();
                commands.Log += LogAsync;

                client.Ready += async () => { await commands.RegisterCommandsGloballyAsync(true); };
                await services.GetRequiredService<BotHandlingService>().InitializeAsync();

                //Starting bot
                await client.LoginAsync(TokenType.Bot, Environment.GetEnvironmentVariable("token"));
                await client.StartAsync();

                //Suspending main program
                await Task.Delay(Timeout.Infinite);
            }
        }

        /// <summary>
        /// Bot logging command.
        /// </summary>
        /// <param name="message">Message to write</param>
        /// <returns>Async command completed state</returns>
        private static Task LogAsync(LogMessage message)
        {
            Console.WriteLine(message.Message);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Creating all the needed services.
        /// </summary>
        /// <returns>Service provider for dependency injection</returns>
        private static ServiceProvider ConfigureServices() => new ServiceCollection()
            .AddSingleton(new DiscordSocketClient(new DiscordSocketConfig
            {
                GatewayIntents = GatewayIntents.AllUnprivileged | GatewayIntents.GuildMembers
            }))
            //.AddSingleton<CommandService>()
            .AddSingleton(x => new InteractionService(x.GetRequiredService<DiscordSocketClient>()))
            .AddSingleton<BotHandlingService>()
            .AddSingleton<HttpClient>()
            .AddDbContext<ABDbContext>(options => options.UseSqlite("Data Source=AB.db"))
            //.AddSingleton<SavedRolesService>()
            .BuildServiceProvider();
    }
}